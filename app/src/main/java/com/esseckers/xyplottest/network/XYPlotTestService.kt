package com.esseckers.xyplottest.network

import retrofit2.Response
import retrofit2.http.*

interface XYPlotTestService {

    @GET("api/test/points")
    suspend fun getPoints(
        @Header("Content-Type") type: String = "application/json",
        @Query("count") count: Int
    ): Response<PointsResponse>
}
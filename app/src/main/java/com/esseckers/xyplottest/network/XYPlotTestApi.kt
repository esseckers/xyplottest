package com.esseckers.xyplottest.network

import com.esseckers.xyplottest.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object XYPlotTestApi {

  private const val OKHTTP_CONNECT_TIMEOUT_MS = 45_000L
  private const val OKHTTP_READ_TIMEOUT_MS = 45_000L

  private fun getRetrofitBuilder(): Retrofit {
    return Retrofit.Builder()
      .client(createOkHttpClient())
      .baseUrl(BuildConfig.XYPLOTTEST_URL)
      .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
      .build()
  }

  val apiService: XYPlotTestService by lazy { getRetrofitBuilder().create(XYPlotTestService::class.java) }

  private fun createOkHttpClient(): OkHttpClient =
    OkHttpClient.Builder()
      .connectTimeout(OKHTTP_CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS)
      .readTimeout(OKHTTP_READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
      .addInterceptor(
        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
          .setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
          )
      ).build()
}
package com.esseckers.xyplottest.network

sealed class NetworkRequestStatus<out T> {
    data class Success<T>(val data: T) : NetworkRequestStatus<T>()
    data class Error(val error: String) : NetworkRequestStatus<Nothing>()
    data class Loading(val isLoading: Boolean) : NetworkRequestStatus<Nothing>()
    object None : NetworkRequestStatus<Nothing>()
}
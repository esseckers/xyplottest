package com.esseckers.xyplottest.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.esseckers.xyplottest.R
import com.esseckers.xyplottest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(this.root)
            setSupportActionBar(this.toolbar)
        }

        (supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment)
            .navController.apply {
                navController = this
                appBarConfiguration = AppBarConfiguration(this.graph)
                setupActionBarWithNavController(this, appBarConfiguration)
            }
    }

    override fun onSupportNavigateUp(): Boolean =
        navController.run {
            navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
        }
}
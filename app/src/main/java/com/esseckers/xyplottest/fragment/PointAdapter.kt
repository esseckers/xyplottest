package com.esseckers.xyplottest.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.esseckers.xyplottest.R
import com.esseckers.xyplottest.network.Point
import kotlinx.android.synthetic.main.item_point.view.*

class PointAdapter : ListAdapter<Point, RecyclerView.ViewHolder>(ItemListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_point, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.tvNumberOfRow.text = position.inc().toString()
        holder.itemView.tvPointXValue.text = item.x.toString()
        holder.itemView.tvPointYValue.text = item.y.toString()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ItemListDiffCallback : DiffUtil.ItemCallback<Point>() {
        override fun areItemsTheSame(oldItem: Point, newItem: Point): Boolean =
            oldItem.x == newItem.x && oldItem.y == newItem.y

        override fun areContentsTheSame(
            oldItem: Point,
            newItem: Point
        ): Boolean =
            oldItem.x == newItem.x && oldItem.y == newItem.y
    }
}
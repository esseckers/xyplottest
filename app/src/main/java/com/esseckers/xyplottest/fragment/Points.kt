package com.esseckers.xyplottest.fragment

import android.os.Parcelable
import com.esseckers.xyplottest.network.Point
import kotlinx.android.parcel.Parcelize

@Parcelize
class Points : ArrayList<Point>(), Parcelable
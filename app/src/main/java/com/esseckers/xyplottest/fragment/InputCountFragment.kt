package com.esseckers.xyplottest.fragment

import androidx.core.text.isDigitsOnly
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.esseckers.xyplottest.R
import com.esseckers.xyplottest.enable
import com.esseckers.xyplottest.hideKeyboard
import com.esseckers.xyplottest.network.NetworkRequestStatus
import com.esseckers.xyplottest.network.Point
import com.esseckers.xyplottest.viewmodel.InputCountViewModel
import com.esseckers.xyplottest.visible
import kotlinx.android.synthetic.main.input_count_fragment.*

class InputCountFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.input_count_fragment

    private val viewModel: InputCountViewModel by viewModels()

    override fun registerObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.pointsData.collect {
                when (it) {
                    is NetworkRequestStatus.Loading -> {
                        progress.visible(it.isLoading)
                        btnGo.enable(!it.isLoading)
                    }
                    is NetworkRequestStatus.Error -> {
                        showError(it.error)
                    }
                    is NetworkRequestStatus.Success<List<Point>> -> {
                        findNavController().navigate(
                            InputCountFragmentDirections.actionInputCountFragmentToPlotFragment(
                                Points().apply {
                                    addAll(it.data)
                                })
                        )
                    }
                    else -> {}
                }
            }
        }
    }

    override fun updateView() {
        etCount.addTextChangedListener {
            btnGo.isEnabled = it?.isNotEmpty() == true
        }

        btnGo.setOnClickListener {
            etCount.text?.let {
                if (it.isDigitsOnly()) {
                    hideKeyboard()
                    viewModel.getPoints(it.toString().toInt())
                } else {
                    showError("Please input only numeric symbols")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModelStore.clear()
    }
}
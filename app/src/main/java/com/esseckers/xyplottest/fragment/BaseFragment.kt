package com.esseckers.xyplottest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.esseckers.xyplottest.hideKeyboard


abstract class BaseFragment : Fragment() {

    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyArguments()
        registerObservers()
    }

    open fun applyArguments() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(layoutRes, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateView()
    }

    open fun registerObservers() {}
    open fun updateView() {}

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    protected fun showError(errorText: String) {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Oops!")
            setMessage(errorText)
            setPositiveButton("Ok") { dialog, _ ->
                dialog.dismiss()
            }
        }.show()
    }
}
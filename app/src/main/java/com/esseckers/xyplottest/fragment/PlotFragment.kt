package com.esseckers.xyplottest.fragment

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.esseckers.xyplottest.R
import com.esseckers.xyplottest.viewmodel.PlotViewModel
import kotlinx.android.synthetic.main.plot_fragment.*
import kotlin.math.abs


class PlotFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.plot_fragment

    private val args: PlotFragmentArgs by navArgs()

    private val viewModel: PlotViewModel by viewModels()

    override fun registerObservers() {
        viewModel.pointsData.observe(requireActivity()) {
            ivPlot.setImageBitmap(it)
        }
    }

    override fun updateView() {
        rvPoints.adapter = PointAdapter().apply {
            submitList(args.points.sortedBy { point -> abs(point.x) })
        }

        viewModel.createPlot(args.points)
    }
}
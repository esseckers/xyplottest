package com.esseckers.xyplottest.repository

import com.esseckers.xyplottest.network.NetworkRequestStatus
import com.esseckers.xyplottest.network.Point
import com.esseckers.xyplottest.network.XYPlotTestApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response

class XYPlotTestRepository {

    fun getPoints(count: Int): Flow<NetworkRequestStatus<List<Point>>> {
        return flow {
            val pointsResponse =
                XYPlotTestApi.apiService.getPoints(count = count)
            if (!pointsResponse.isSuccessful) {
                emitHttpError(pointsResponse)
                return@flow
            }
            pointsResponse.body()?.let {
                emit(NetworkRequestStatus.Success(it.points))
            } ?: emit(NetworkRequestStatus.Error("List is empty"))
        }.flowOn(Dispatchers.IO)
    }

    private suspend fun FlowCollector<NetworkRequestStatus<List<Point>>>.emitHttpError(
        errorResponse: Response<*>
    ) {
        val errorMsg = errorResponse.errorBody()?.string()
        errorResponse.errorBody()?.close()

        errorMsg?.let {
            emit(NetworkRequestStatus.Error(it))
        } ?: emit(
            NetworkRequestStatus.Error(
                "Something went wrong, check input data or internet connection"
            )
        )
    }
}
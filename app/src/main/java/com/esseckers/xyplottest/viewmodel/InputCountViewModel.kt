package com.esseckers.xyplottest.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.esseckers.xyplottest.network.NetworkRequestStatus
import com.esseckers.xyplottest.network.Point
import com.esseckers.xyplottest.repository.XYPlotTestRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class InputCountViewModel : ViewModel() {

    private val repository: XYPlotTestRepository by lazy {
        XYPlotTestRepository()
    }

    private val internalPointsData =
        MutableStateFlow<NetworkRequestStatus<List<Point>>>(NetworkRequestStatus.None)
    val pointsData: StateFlow<NetworkRequestStatus<List<Point>>> get() = internalPointsData

    fun getPoints(count: Int) = viewModelScope.launch {
        commonFlow(
            repository.getPoints(count),
            internalPointsData
        )
    }

    private suspend fun commonFlow(
        flow: Flow<NetworkRequestStatus<List<Point>>>,
        stateFlow: MutableStateFlow<NetworkRequestStatus<List<Point>>>
    ) = flow
        .onStart { stateFlow.value = NetworkRequestStatus.Loading(true) }
        .catch {   stateFlow.value = NetworkRequestStatus.Error(
            "Something went wrong, check input data or internet connection"
        ) }
        .collect { data ->
            stateFlow.value = NetworkRequestStatus.Loading(false)
            stateFlow.value = data
        }

    override fun onCleared() {
        super.onCleared()
        internalPointsData.value = NetworkRequestStatus.None
    }
}
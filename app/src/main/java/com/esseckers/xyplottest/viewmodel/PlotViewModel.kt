package com.esseckers.xyplottest.viewmodel

import android.graphics.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.esseckers.xyplottest.fragment.Points
import kotlinx.coroutines.launch
import kotlin.math.abs

class PlotViewModel : ViewModel() {

    private val internalPointsData =
        MutableLiveData<Bitmap>()
    val pointsData: LiveData<Bitmap> get() = internalPointsData

    fun createPlot(points: Points) = viewModelScope.launch {
        internalPointsData.postValue(createBitmapPlot(points))
    }

    private fun createBitmapPlot(points: Points): Bitmap =
        Bitmap.createBitmap(350, 250, Bitmap.Config.ARGB_8888).apply {
            val canvas = Canvas(this)

            val paintPlotArea = Paint().apply {
                color = Color.BLACK
                isAntiAlias = true
                style = Paint.Style.STROKE
                strokeWidth = 10f
            }

            val paintPlot = Paint().apply {
                color = Color.RED
                isAntiAlias = true
                style = Paint.Style.STROKE
                strokeWidth = 2f
            }

            canvas.drawLine(
                0f,
                this.height.toFloat(),
                this.width.toFloat(),
                this.height.toFloat(),
                paintPlotArea
            )
            canvas.drawLine(0f, this.height.toFloat(), 0f, 0f, paintPlotArea)

            val path = Path().apply {
                points.sortedBy { point -> abs(point.x) }.forEachIndexed { index, point ->
                    if (index == 0) {
                        moveTo(abs(point.x),300- abs(point.y))
                    } else {
                        lineTo(abs(point.x),250 - abs(point.y))
                    }
                }
            }

            canvas.drawPath(path, paintPlot)
        }
}